# encoding: UTF-8
# language: ru

Given(/^Перейти на страницу "(.*?)"$/) do |page|
  visit page
end

Given(/^Проверить наличие поля заголовка "(.*?)"$/) do |text|
  begin
    find("//h1[contains(text(), '#{text}')]")
  rescue Exception => NoSuchElementError
    puts "Поле элемента с текстом #{text} не найдено!"
  end
end

Given(/^Проверить наличие поля ссылки:"/) do |table|
  data = table.hashes
  data.each {|row|
    row.each {|k, v|
      #puts v
      begin
        find("//a[contains(text(), '#{v}')]")
      rescue Exception => NoSuchElementError
        puts "Поле элемента с текстом #{v} не найдено!"
      end
    }
}
end

Given(/^Проверить наличие поля текста:/) do |table|
  data = table.hashes
  data.each {|row|
    row.each {|k, v|
      #puts v
    begin
      find("//div[contains(text(), '#{v}')]")
    rescue Exception => NoSuchElementError
      puts "Поле элемента с текстом #{v} не найдено!"
    end
    }
  }
end

When(/^Ввести значение "(.*?)" в поле "Стоимость недвижимости"/) do |value|
  find("//input[@name='cost']").set(value)
end

And(/^Выбрать значение "(.*?)" в выпадающем списке рядом с полем "Первоначальный взнос"/) do |option|
  find("//select[@name='start_sum_type']").find("//option[contains(text(), '#{option}')]").select_option
end

And(/^Ввести значение "(.*?)" в поле "Первоначальный взнос"/) do |value|
  find("//input[@name='start_sum']").set(value)
end

Then(/^Проверить, что в разделе "Первоначальный взнос" появился текст "(.*?)"/) do |value|
  #puts find("//div[@class='calc-input-desc start_sum_equiv']").text
  if find("//div[@class='calc-input-desc start_sum_equiv']").text == value then puts "Значение равно #{value}!!!"
  else puts "Значение не равно #{value}!!!"
  end
end

And(/^Проверить, что "Сумма кредита" равна "(.*?)"/) do |value|
  valueText = find("//span[@class='credit_sum_value text-muted']").text
  currencyText = find("//span[@class='calc-input-desc']").text
  if (valueText + " " + currencyText) == value
    then puts "Значение равно #{value}!"
  else puts "Значение не равно #{value}"
  end
end

$months
When(/^Ввести значение "(.*?)" в поле "Срок кредита"/) do |value|
  $months = value.to_f
  find("//input[@name='period']").set(value)
end

def generateNumber(from, to)
  number = rand(from..to)
  #puts number
  return number
end

$percentBet

And(/^Ввести значение, генерируемое методом generateNumber, в поле "Процентная ставка"/) do
  $percentBet = generateNumber(5, 12)
  find("//input[@name='percent']").set($percentBet)
  $percentBet = $percentBet.to_f
end

And(/^Проверить, что отмечен радиобаттон "Аннуитентные" и не отмечен радиобаттон "Дифференцированные"/) do
  Capybara.ignore_hidden_elements = false
  radio1 = find("//input[@id='payment-type-1' and @type='radio']")
  radio2 = find("//input[@id='payment-type-2' and @type='radio']")
  puts "Радиобаттон 'Аннуитентные' отмечен!" if radio1.checked?
  puts "Радиобаттон 'Дифференцируемые' не отмечен!" if !radio2.checked?
end

When(/^Нажать на кнопку "(.*?)"/) do |text|
  find("//input[contains(@value, '#{text}') and @type='submit']").click
end

def checkCalculating(i, n)
  sumCredit = find("//span[@class='credit_sum_value text-muted']").text.gsub!(/\s+/, '').to_f
=begin
  puts "sumcredit = #{sumCredit}"
  puts "i = #{i}"
=end
  return (sumCredit * i * (1 + i)**n) / ((1 + i)**n - 1)
end

Then(/^Проверить, что значение ежемесячного платежа соответствует значению, расчитанному по формуле/) do
  sleep(2)
  monthlyPayment = find("//div[contains(@class, 'result-placeholder-monthlyPayment')]").text.gsub!(',', '.').gsub!(/\s+/, '').to_f
  $months *= 12 if find("//select[@name='period_type']").find("//option[@value='Y']").selected?
  $percentBet /= 1200
=begin
  puts "mounths = #{$months}"
  puts "percentBet = #{$percentBet}"
  puts monthlyPayment
  puts checkCalculating($percentBet, $months).round(2)
=end
  checkCalc = checkCalculating($percentBet, $months).round(2)
  if monthlyPayment == checkCalc then puts "Ежемесячный платеж совпадает с расчетом по формуле!"
  else puts "Ежемесячный платеж не совпадает с расчетом по формуле!"
  end
end