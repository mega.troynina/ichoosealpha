require 'capybara/cucumber'
require 'selenium-webdriver'

Capybara.register_driver :driver do |app|
  case ENV['DRIVER']
  when 'chrome'
    Selenium::WebDriver::Chrome::Service.driver_path = "C:/chromedriver/chromedriver.exe"
    Capybara::Selenium::Driver.new(app, :browser => :chrome)
  when 'without_browser'
    Capybara.default_driver = :mechanize
  end
end

Capybara.default_driver = :driver
Capybara.default_selector = :xpath
